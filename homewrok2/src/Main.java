import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите год в формате yyyy: ");
        int year = scanner.nextInt();

        if (year % 400 == 0){
            System.out.println("Результат проверки: 366 дней в году, это високосный год");
        }else if (year % 400 != 0){
            System.out.println("Результат проверки: 365 дней в году, это не високосный год");
        }

    }
}